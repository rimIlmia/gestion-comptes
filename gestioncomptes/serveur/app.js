//'application requiert l'utilisation de modules.
const express = require("express");
//passer les contenu les données envoyées (formulaire)
const bodyParser = require("body-parser");
// travailler sur un fichier
const fs = require("fs");
// travailler sur un fichier jsonfile
const jsonfile = require("jsonfile");
//Mon fichier JSON
const fichier = "users.json";
// Les paramètres du serveur.
var hostname = "localhost";
var port = 3000;
// Nous créons un objet Express.
var app = express();
//Afin de faciliter le routage (les URL que nous souhaitons prendre en charge dans notre API), nous créons un objet Router.
//C'est à partir de cet objet myRouter, que nous allons implémenter les méthodes.
var myRouter = express.Router();
//Dire à mon serveur qu'il va utiliser bodyparser pour mes données reçu (req+param)
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
/*****Liste utilisateur**/
myRouter.route("/user").get(function(req, res) {
  fs.readFile(fichier, "utf8", (err, jsonString) => {
    if (err) {
      //En cas d'erreur de lecture
      console.log("Erreur de lecture de mon fichier:", err);
      return;
    }
    try {
      /**Avec fs quand on recupere les données du fichier, le resultat est de type string et il faut
 la parser en objet pour qu'on puisse y acceder**/
      //Parser mes données JSON en objet pour pouvoir utiliser
      let liste = JSON.parse(jsonString);
      //Envoyer en données mon objet d'objets
      res.send(liste);
    } catch (err) {
      //Erreur de parse de mes données JSON en objet
      console.log("Error parsing JSON string:", err);
    }
  });
});
/**************Ajouter un utilisateur***************/
myRouter.route("/user/new").post(function(req, res) {
  //Je commence par lire mon fichier
  fs.readFile(fichier, "utf8", (err, jsonString) => {
    if (err) {
      //En cas d'erreur de lecture de mon fichier (fichier endomagé)
      console.log("Erreur lecture de fichier", err);
      return;
    }
    try {
      //Recuperr mes données json et les parser ou convertir en objet d'objet
      let customer = JSON.parse(jsonString);
      //Ajouter un new objet à cet objet d'objets avec la methode push
      customer.contact.push(req.body);
      //Enregister dans mon fichier (j'utilise write parceque je veux ecraser le contenu de l'ancien fichier)
      fs.writeFile(fichier, JSON.stringify(customer, null, 2), function(err) {
        if (err) throw err;
        //Dans le cas où j'arrive à mettre à jour mon fichier
        console.log("Saved!");
        //J'envoie un OK à mon serveur
        res.send("Client ajouté");
      });
    } catch (err) {
      //En cas d'erreur de passage
      console.log("Error parsing JSON string:", err);
    }
  });
});
/*******************Supprimer un user******************/
// Liste des users
myRouter
  .route("/supprimer")
  // J'implémente la methode get
  .post(function(req, res) {
    //Lire mon fichier
    console.log("je suis demander");
    fs.readFile(fichier, "utf8", (err, jsonString) => {
      if (err) {
        console.log("Error reading file from d² &isk:", err);
        return;
      }
      try {
        /**Avec fs quand on recupere les données du fichier, le resultat est de type string et il faut
 la parser en objet pour qu'on puisse y acceder**/

        let customer = JSON.parse(jsonString);
        let ojid = JSON.stringify(req.body);
        let id = JSON.parse(ojid);
        console.log("La personne a supprimer" + id.id);

        customer.contact.splice(parseInt(id.id), 1);
        var data = JSON.stringify(customer);

        fs.writeFile(fichier, data, function(err) {});
        res.send("lidentifiant:" + req.body + "apres" + data);
      } catch (err) {
        console.log("Error parsing JSON string:", err);
      }
    });
  });

//Nous demandons à l'application d'utiliser notre routeur
app.use(myRouter);
// Démarrer le serveur
app.listen(port, hostname, function() {
  console.log("Mon serveur API fonctionne sur http://" + hostname + ":" + port);
});

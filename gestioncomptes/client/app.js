//Express pour le routage
const express = require("express");
//Ejs pour les template
const ejs = require("ejs");
//pour parser la requette
const bodyParser = require("body-parser");
// utiliser axios pour acceder à mon api
const axios = require("axios");
//Creer un objet express
const app = express();
// Les paramètres du serveur.
var hostname = "localhost";
var port = 2000;
//Definir le fichier views
app.set("views", "./views");
app.set("view engine", "ejs");
//utilisation de express static pour le repertoire racine comme ca il reconnait les dossier img css js
app.use(express.static(__dirname + "/"));
//utilisation de bodyParser pour parser les données passées dans la requete
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
//Definition de l'url de mon api
const urlApi = "http://localhost:3000";
/*** Definition des routes ***/
// Aller page index: quand le visiteur tape / dans son navigateur

app.get("/", function(req, res) {
  //renvoi la page index
  res.render("index");
});
//page Apropos
app.get("/apropos", function(req, res) {
  //renvoi la page index
  res.render("apropos");
});
//Aller sur la page index quand le visiteur tape /index
app.get("/index", function(req, res) {
  res.render("index");
});
// aller page liste qui affiche la liste des utilisateurs
app.get("/users", async function(req, res) {
  let contacts = await axios.get(urlApi + "/user");
  console.log(contacts); //affiche le contenu de la requete
  let contactss = JSON.stringify(contacts.data.contact);
  let contactob = JSON.parse(contactss);

  res.render("liste", { resultat: contactob });
});
// Aller page Formulaire
app.get("/formulaire", function(req, res) {
  res.render("formulaire");
});
//Ajouter un user
app.post("/users", function(req, res) {
  // faire une demande à axios en lui envoyant les données de mes formulaire (req.body)
  console.log("req" + JSON.stringify(req.body));
  axios
    .post(urlApi + "/user/new", req.body)
    .then(function(response) {
      //En cas succes de rajout de mon client je vais faire un get à mon api
      //pour qu'elle m'envoi le fichier apres mise à jour
      axios
        .get(urlApi + "/user")
        .then(function(response) {
          //Je parse mes data en string
          let donnees = JSON.stringify(response.data.contact);
          console.log("donnee a rajouter" + donnees);
          //Je les parse en tableau d'objets pour pour voir acceder à leurs proprietes
          let contactob = JSON.parse(donnees);
          //un fois tout est bon j'envoi tout à la page liste avec mon tableau d'objet
          res.render("liste", { resultat: contactob });
        })
        .catch(function(error) {
          // En cas d'erreur de la demande de la nouvelle liste
          console.log("Erreur de demande de liste: " + error);
        });
    })
    .catch(function(error) {
      //En cas d'erreur d'enregistrement
      console.log("erreur d'enregistrement" + error);
    });
});
/**Supprimer un utilisateur*****/
app.get("/supprimer:id", function(req, res) {
  console.log("id:" + req.params);
  axios
    .post(urlApi + "/supprimer", { id: req.params.id })
    .then(function(response) {
      console.log("user delete");
      res.render("index");
    })
    .catch(function(error) {
      console.log(error);
    });
});
//Mon serveur en ecoute et il attends les demande
app.listen(port, hostname, function() {
  console.log(
    "Mon serveur client fonctionne sur http://" + hostname + ":" + port
  );
});
